class Parcel
  include Mongoid::Document
  include Mongoid::Timestamps

  field :parcel_code, type: String
  field :api_response, type: Array

  belongs_to :service, inverse_of: :parcels
end
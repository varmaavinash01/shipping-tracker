class Service
  include Mongoid::Document
  field :name, type: String
  field :info, type: String
  field :link, type: String
  field :code, type: String
  field :meta, type: String

  has_many :parcels, inverse_of: :parcels
end

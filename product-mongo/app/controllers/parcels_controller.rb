class ParcelsController < ApplicationController
  before_action :set_parcel, only: [:show, :update, :destroy]

  # GET /parcels
  def index
    @parcels = Parcel.all

    render json: @parcels
  end

  # GET /parcels/1
  def show
    render json: @parcel
  end

  # POST /parcels
  def create
    @parcel = Parcel.new(parcel_params)

    if @parcel.save
      render json: @parcel, status: :created, location: @parcel
    else
      render json: @parcel.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /parcels/1
  def update
    if @parcel.update(parcel_params)
      render json: @parcel
    else
      render json: @parcel.errors, status: :unprocessable_entity
    end
  end

  # DELETE /parcels/1
  def destroy
    @parcel.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_parcel
      resp = Tracker::Base.execute no: params[:id], company: params[:service_id]
      @parcel = Parcel.find_by(parcel_code: params[:id])
      if @parcel.present?
        @parcel.api_response = resp[0]
        @parcel.save!
      else
        @parcel = Parcel.new
        @parcel.parcel_code = params[:id]
        @parcel.service = Service.find_by(code: params[:service_id])
        @parcel.api_response = resp[0]
        @parcel.save!
      end
    end

    # Only allow a trusted parameter "white list" through.
    def parcel_params
      params.require(:parcel).permit(:parcel_code, :api_response)
    end
end

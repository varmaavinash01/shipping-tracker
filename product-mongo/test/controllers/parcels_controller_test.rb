require 'test_helper'

class ParcelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @parcel = parcels(:one)
  end

  test "should get index" do
    get parcels_url, as: :json
    assert_response :success
  end

  test "should create parcel" do
    assert_difference('Parcel.count') do
      post parcels_url, params: { parcel: { api_response: @parcel.api_response, parcel_id: @parcel.parcel_id } }, as: :json
    end

    assert_response 201
  end

  test "should show parcel" do
    get parcel_url(@parcel), as: :json
    assert_response :success
  end

  test "should update parcel" do
    patch parcel_url(@parcel), params: { parcel: { api_response: @parcel.api_response, parcel_id: @parcel.parcel_id } }, as: :json
    assert_response 200
  end

  test "should destroy parcel" do
    assert_difference('Parcel.count', -1) do
      delete parcel_url(@parcel), as: :json
    end

    assert_response 204
  end
end

class Tracker < Neo4j::Migrations::Base
  def up
    add_constraint :Tracker, :uuid
  end

  def down
    drop_constraint :Tracker, :uuid
  end
end

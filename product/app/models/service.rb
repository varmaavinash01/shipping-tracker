class Service 
  include Neo4j::ActiveNode
  property :id, type: String
  property :name, type: String
  property :info, type: String
  property :link, type: String
  property :code, type: String
  property :meta, type: String

  has_many :in, :packages, origin: :Tracker

end

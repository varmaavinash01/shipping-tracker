class Tracker 
  include Neo4j::ActiveNode
  property :tracker_id, type: String
  property :response, type: String
  property :—timestamps, type: String
   
  has_one  :out, :service, model_class: :Service
end
